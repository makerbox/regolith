Capacitive Hardware Trigger
=============================

For electronic paintings done by artist Jess Weichler; this
application (internally known as "Regolith") drives the Raspberry Pi,
which currently receives signals from a Makey Makey Go, so that
external hardware events are triggered via GPIO when a circuit is
completed.

This uses the Makey Makey Go because it's cheaper than the Makey Makey
but basically just as easy to use. A potential drawback, if you are
looking into using this yourself, is a delay in the trigger (you
complete the circuit, and blink, and THEN the event starts). We didn't
care about this, so we went for it. If you are looking to make your
own Makey Makey interface for cheap and don't mind the extra work
involved, check out the `MPR121 touch sensor
<https://www.adafruit.com/product/1982>`_ (I have not tested it yet,
so I can't guarantee it's quicker to respond, but I'll bet it is).


Dependencies
---------------

Python
`RPi.GPIO <https://pypi.python.org/pypi/RPi.GPIO>`_
`Python Evdev <https://pypi.python.org/pypi/evdev>`_

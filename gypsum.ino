// gypsum for umbrella part
// GPLv3 by Klaatu

#include <Adafruit_NeoPixel.h>
#include <stdlib.h>
#define PIN    6
#define N_LEDS 69 // 5 meter reel @ 30 LEDs/m

Adafruit_NeoPixel strip = Adafruit_NeoPixel(N_LEDS, PIN, NEO_GRB + NEO_KHZ800);

struct Light_strip {
 uint32_t color;
 uint16_t offset;
}; 

const uint16_t light_length = strip.numPixels();
const uint16_t DROPLET = 3;
const int buttonPin = 2;
int buttonState = 0;

void setup() {
  Serial.begin(9600);
  strip.begin();
  strip.show(); // Init px OFF
}

void chase(uint32_t c, uint8_t wait) {
  for (int j=35; j<light_length; j++) {
    // error catching
        uint16_t k = j-35;
        strip.setPixelColor(66, 0);
        strip.setPixelColor(67, 0);
        strip.setPixelColor(31, 0);
    //droplets
    if (k > 31){strip.setPixelColor(31, c);strip.setPixelColor(33, c);}
    strip.setPixelColor(j, c);
    strip.setPixelColor(k, c);
    strip.setPixelColor(j-3, 0);
    strip.setPixelColor(k-3, 0);
    strip.show();delay(wait);

}}

void loop() {
  buttonState = digitalRead(buttonPin);
  if (buttonState == HIGH) {     
    // turn LED on:       
    Serial.println("hit");
chase(0x19b3b7,39);
  } 
  else {
    // turn LED off:
chase(0x000000,39);
  }
}


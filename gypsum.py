#!/usr/bin/env python
# copyright klaatu @member dot fsf dot org
##### GPLv3
# This program is free software: you can redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, either version 3 of the License, 
# or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR  PURPOSE.  See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public  License along with this program.  If not, see 
# http://www.gnu.org/licenses/

import pygame
from pygame.locals import *
import os
import time
import RPi
import RPi.GPIO as GPIO
import logging
from   evdev  import  InputDevice, categorize, ecodes
from   select import  select

# look how easy logging is
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.FileHandler(os.path.join("/","tmp","gypsum.log"))
handler.setLevel(logging.INFO)
logger.addHandler(handler)

logger.info("Start gypsum logging.")

pygame.mixer.init()
GPIO.setmode(GPIO.BCM)
lightshow = 2

main = True
logger.debug("Searching for Input Event0.")
while main == True:
    try:
        dev = InputDevice('/dev/input/event0')
        print("debug",dev,str(time.time()).split('.')[0])
        main = False
    except:
        # gives  time  to disconnect
        print("waiting for input device...")
        logger.debug("waiting input device...")
        # keyboard and plug in makey
        time.sleep(9)

print('breakout!!')
logger.debug("breakout!")
GPIO.setup( lightshow,GPIO.OUT)
pygame.mixer.music.load(os.path.join('/','usr','local','bin','gypsum','a.ogg'))
hit = False

main = True
while main == True:
    if hit:
        if not pygame.mixer.music.get_busy():
            pygame.mixer.music.play(1)

    r,w,x = select([dev], [], [])
    for event in dev.read():
        if event.type == ecodes.EV_KEY:
            APRESS = categorize( event)
            ADICT  = str(APRESS).split(" ")
            try:
                if ADICT[7] == "hold":
                    #print(ADICT[7])
                    GPIO.output(lightshow,True)
                    hit = True
                    logger.debug("hit TRUE hold")
                elif ADICT[7] == "down":
                    GPIO.output(lightshow,True)
                    print(ADICT[7])
                    hit = True
                    logger.debug("hit TRUE down")
                else:
                    print(ADICT[7])
                    GPIO.output(lightshow,False)
                    if pygame.mixer.music.get_busy():
                        pygame.mixer.music.fadeout(1212)
                    hit = False
                    logger.debug("hit FALSE else")
            except Exception:
                print('Exception')
                logger.debug("exception in even loop")


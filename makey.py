#!/usr/bin/env python
# copyright klaatu @member dot fsf dot org
#
# GPLv3
# This program is free software: you can redistribute it 
# and/or modify it under the terms of the 
# GNU General Public License as published by the 
# Free Software Foundation, either version 3 of the License, 
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be 
# useful, but WITHOUT ANY WARRANTY; without even the implied 
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public 
# License along with this program.  If not, see 
# http://www.gnu.org/licenses/

#import pygame
#from pygame.locals import *
import os
import RPi
import time
import RPi.GPIO as GPIO
from   evdev  import  InputDevice, categorize, ecodes
from   select import  select

GPIO.setmode(GPIO.BCM)

# start servo subsystem
light = 2 #3.3  volt phys 3
l2ght = 3 #3.3  volt phys 5
tom   = 0  #  servo 
#light = 2 #  5  volt
try:
    os.system('/usr/local/bin/regolith/servod')
except:
    print('Servod command not found.')

if os.path.exists('/dev/servoblaster'):
    print('found /dev/servoblaster')
    os.system('echo '+str(tom)+'=50 > /dev/servoblaster')
    os.system('echo '+str(tom)+'=250 > /dev/servoblaster')
else:
    print('no servoblaster found')

while True:
    try:
        if os.path.exists('/dev/input/event0'):
            GPIO.setup( light,GPIO.OUT)
            GPIO.setup( l2ght,GPIO.OUT)
            GPIO.output(light,False   )
            GPIO.output(l2ght,False   )
            dev = InputDevice('/dev/input/event0')
            print("debug",dev,str(time.time()).split('.')[0])
            hit = False

            main = True
            while main == True:
                #print('top')#debug
                #print(str(hit))

                if not hit:
                    GPIO.output( light,False )
                    GPIO.output( l2ght,False )
                    try:
                        os.system('echo '+str(tom)+'=50 > /dev/servoblaster')
                    except:
                        print('no, i cant do that')
                    #print('off')
                else:
                    print('on' )
                    GPIO.output( light, True )
                    GPIO.output( l2ght, True )

                r,w,x = select([dev], [], [])
                for event in dev.read():
                    #print('in')
                    if event.type == ecodes.EV_KEY:
                        APRESS = categorize( event)
                        ADICT  = str(APRESS).split(" ")
                        print(ADICT)[4]
                        hit = True
                        print(str(hit))
                        #SERVO
                        try:
                            os.system('echo '+str(tom)+'=250 > /dev/servoblaster')
                        except:
                            print('no can do')
                        print('hit' )
                    else:
                        #print('out')
                        hit = False
                        break
                    break

        else:
            time.sleep(3)

    except:
        # gives  time  to disconnect
        # keyboard and plug in makey
        time.sleep(9)

